#!usr/bin/python

import os
from detectionapi import DetectionApi
import argparse
import warnings

warnings.filterwarnings("ignore")


def options():

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='Commands')

    detect_parser = subparsers.add_parser(
        'detect', help='Given a plant image as input, it show what type of disease the plant have')

    detect_parser.add_argument(
        'input', type=str, help='Input image', action='store')

    args = vars(parser.parse_args())

    return args


def main():

    args = options()

    if 'input' in args:

        img_test_path = args['input']

        if os.path.exists(img_test_path):
            detect_disease = DetectionApi(img_test_path)

            print(detect_disease.predict())
        else:
            print("Files or directory doesn't exist")
            exit(0)
    else:
        print("Arguments not found")
        exit(0)


if __name__ == '__main__':
    main()
