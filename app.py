import os
import urllib.request
from flask import Flask, request, redirect, render_template, jsonify
from werkzeug.utils import secure_filename
from detectionapi import DetectionApi
from model_db import ModelDb
import time
import uuid

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

UPLOAD_FOLDER = 'saved_imgs'

app = Flask(__name__)
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def index():
    return render_template('upload.html')


@app.route('/detect', methods=['POST'])
def upload_file():
    if request.method == "POST":
        # check if the post request has the file part
        if 'file' not in request.files:
            resp = jsonify({'message': 'No file part in the request'})
            resp.status_code = 400
            return resp
        file = request.files['file']
        if file.filename == '':
            resp = jsonify({'message': 'No file selected for uploading'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            unique_filename = uuid.uuid4().hex
            filename, file_extension = os.path.splitext(filename)
            unique_filename += file_extension
            file_path = os.path.join(
                app.config['UPLOAD_FOLDER'], unique_filename)
            file.save(file_path)

            detect_disease = DetectionApi(file_path)
            predicted_resp = detect_disease.predict()

            time_now = time.strftime('%Y-%m-%d %H-%M-%S')
            agent = str(request.headers.get('User-Agent'))
            ipv4 = str(request.remote_addr)
            label_predicted = predicted_resp['plant'] + \
                ":"+predicted_resp['status']
            ip6_adr = ""
            label_gdt = ""

            val = (time_now, ipv4, ip6_adr, agent,
                   file_path, label_predicted, label_gdt)

            try:
                db_plant = ModelDb()
                db_plant.add_image(val)

                resp = jsonify({'message': predicted_resp})
                db_plant.end_cnx()
                resp.status_code = 201
                return resp
            except:
                resp = jsonify(
                    {'message': 'Insertion in database was not sucessuful'})
                resp.status_code = 400
                return resp

        else:
            resp = jsonify(
                {'message': 'Allowed file png, jpg, jpeg, gif'})
            resp.status_code = 400
            return resp


if __name__ == "__main__":
    app.run()
