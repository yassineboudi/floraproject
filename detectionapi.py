import os
import os.path
import torch.utils.data as data
import torch
from PIL import Image
from torchvision import transforms
from torch.autograd import Variable
import json


class DetectionApi():

    def __init__(self, image_path):

        self.device = "cpu"
        self.model_path = "models/plant.pth"
        self.model = torch.load(self.model_path)

        self.class_names_path = "config/class_names.json"
        self.image_path = image_path

    def predict(self):

        self.model.eval()
        data_transforms = transforms.Compose([
            transforms.Resize(size=256),
            transforms.CenterCrop(size=224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [
                0.229, 0.224, 0.225])
        ])

        with open(self.class_names_path) as json_file:
            class_names = json.load(json_file)[0]

        image_tensor = data_transforms(Image.open(self.image_path)).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input = Variable(image_tensor)
        input = input.to(self.device)
        output = self.model(input)
        index = output.data.cpu().numpy().argmax()

        return class_names[index]
