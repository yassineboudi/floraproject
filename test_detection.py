#!usr/bin/python

from detectionapi import DetectionApi
import pytest


def test_detect_disease():
    img_test_path = "test/apple_cedar_apple_rust.JPG"
    detect_disease = DetectionApi(img_test_path)
    assert detect_disease.predict() == {
        'plant': 'Apple', 'status': 'cedar apple rust'}
