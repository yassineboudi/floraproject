import warnings
import mysql.connector

warnings.filterwarnings("ignore")


class ModelDb():

    def __init__(self):

        self.mydb = mysql.connector.connect(
            host="",
            user="",
            passwd="",
            database=""
        )

        self.mycursor = self.mydb.cursor()

    def add_image(self, val):

        sql = ""
        self.mycursor.execute(sql, val)
        self.mydb.commit()

    def end_cnx(self):
        return self.mydb.close()
