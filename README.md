# FloraProject

A program that can detect and diagnose the health of a plant (leaf) based on deep neural networks. Its includes 14 types of plant and over 30 deceases (spots, sour, rust).

### Prerequisites

OS tested:

- Ubuntu 16.04

Dependencies:

- pytorch 1.3.1

## Usage

The program has two modes:

1. Standalone app: a console application.

2. Web app: based on Flask in the backend, jQuery in the frontend.

### Standalone app

```
python standalone.py detect <image_path>
```

#### Inputs:

- `<image_path>` : image path.

#### Output:

- `<output>` : It return the type of the plant and decease if there is one.

#### Example:

```
python standalone.py detect test/apple_cedar_apple_rust.JPG
```

### Web app

```
python server.py
```

#### Example:

After launching the previous command; enter the following adress in the browser: http://127.0.0.1:5000/ and start uploading your images by drop and drag mechanism

## Test

```
pytest
```

## Demo

[Click me](https://flora.yassineboudi.com)

## Dataset

The data used for this app is available at: https://www.kaggle.com/vbookshelf/v2-plant-seedlings-dataset

## References

https://pytorch.org

https://www.kaggle.com

## Authors

Yassine Boudi
